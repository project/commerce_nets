<?php

/**
 * @file
 * Capture, credit and cancel admin interface.
 */

/**
 * Cancel: form handler.
 */
function commerce_nets_admin_cancel($form, $form_state, $order, $transaction) {
  module_load_include('inc', 'commerce_nets');
  $uri = commerce_payment_ui_payment_transaction_uri($transaction);

  $form['order'] = array(
    '#type' => 'value',
    '#value' => $order,
  );
  $form['transaction'] = array(
    '#type' => 'value',
    '#value' => $transaction,
  );

  return confirm_form($form, t('Would you like to cancel this transaction?'), $uri['path']);
}

/**
 * Cancel: submit handler.
 */
function commerce_nets_admin_cancel_submit($form, &$form_state) {
  module_load_include('inc', 'commerce_nets');
  $order = $form_state['values']['order'];
  $transaction = $form_state['values']['transaction'];
  $uri = commerce_payment_ui_payment_transaction_uri($transaction);

  $payment_method = commerce_payment_method_instance_load($transaction->instance_id);

  if (!$payment_method) {
    drupal_set_message(t('Failure to load Payment method (@instance_id) for transaction.', array('@instance_id' => $transaction->instance_id)), 'error');
    $form_state['rebuild'] = TRUE;
  }
  elseif ($api_call_success = commerce_nets_process_transaction($order, $payment_method, $transaction, 'ANNUL')) {
    // Set transaction status to what Nets told us it did.
    $transaction->status = commerce_nets_transaction_status_from_nets_operation('ANNUL');
    commerce_payment_transaction_save($transaction);

    drupal_set_message(t('Transaction has been canceled.', array()));
    $form_state['redirect'] = $uri['path'];
  }
  else {
    drupal_set_message(t('Unable to cancel transaction. %message', array('%message' => $transaction->payload['%message'])), 'error');
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Capture: form handler.
 */
function commerce_nets_admin_capture($form, $form_state, $order, $transaction) {
  module_load_include('inc', 'commerce_nets');
  $uri = commerce_payment_ui_payment_transaction_uri($transaction);

  $form['markup']['summary'] = commerce_nets_get_payment_summary($transaction);

  $form['order'] = array(
    '#type' => 'value',
    '#value' => $order,
  );
  $form['transaction'] = array(
    '#type' => 'value',
    '#value' => $transaction,
  );
  $form['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount'),
    '#default_value' => commerce_currency_amount_to_decimal(_commerce_nets_amount($transaction, 'CAPTURE'), $transaction->currency_code),
  );

  return confirm_form($form, t('What amount would you like to capture?'), $uri['path']);
}

/**
 * Capture: submit handler.
 */
function commerce_nets_admin_capture_submit($form, &$form_state) {
  module_load_include('inc', 'commerce_nets');
  $order = $form_state['values']['order'];
  $transaction = $form_state['values']['transaction'];
  $uri = commerce_payment_ui_payment_transaction_uri($transaction);

  $payment_method = commerce_payment_method_instance_load($transaction->instance_id);

  if (!$payment_method) {
    drupal_set_message(t('Failure to load Payment method (@instance_id) for transaction.', array('@instance_id' => $transaction->instance_id)), 'error');
    $form_state['rebuild'] = TRUE;
  }
  elseif ($api_call_success = commerce_nets_process_transaction($order, $payment_method, $transaction, 'CAPTURE', commerce_currency_decimal_to_amount($form_state['values']['amount'], $transaction->currency_code))) {
    // Set transaction status to what Nets told us it did.
    $transaction->status = commerce_nets_transaction_status_from_nets_operation('CAPTURE');
    // Update amount - store how much has been captured - credited.
    $transaction->amount = _commerce_nets_amount($transaction, 'CREDIT', TRUE);
    commerce_payment_transaction_save($transaction);

    drupal_set_message(t('Transaction has been captured.', array()));
    $form_state['redirect'] = $uri['path'];
  }
  else {
    drupal_set_message(t('Unable to capture transaction. %message', array('%message' => $transaction->payload['%message'])), 'error');
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Credit: form handler.
 */
function commerce_nets_admin_credit($form, $form_state, $order, $transaction) {
  module_load_include('inc', 'commerce_nets');
  $uri = commerce_payment_ui_payment_transaction_uri($transaction);

  $form['markup']['summary'] = commerce_nets_get_payment_summary($transaction);

  $form['order'] = array(
    '#type' => 'value',
    '#value' => $order,
  );
  $form['transaction'] = array(
    '#type' => 'value',
    '#value' => $transaction,
  );
  $form['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount'),
    '#default_value' => commerce_currency_amount_to_decimal(_commerce_nets_amount($transaction, 'CREDIT'), $transaction->currency_code),
  );

  return confirm_form($form, t('What amount would you like to credit?'), $uri['path']);
}

/**
 * Credit: submit handler.
 */
function commerce_nets_admin_credit_submit($form, &$form_state) {
  module_load_include('inc', 'commerce_nets');
  $order = $form_state['values']['order'];
  $transaction = $form_state['values']['transaction'];
  $uri = commerce_payment_ui_payment_transaction_uri($transaction);

  $payment_method = commerce_payment_method_instance_load($transaction->instance_id);

  if (!$payment_method) {
    drupal_set_message(t('Failure to load Payment method (@instance_id) for transaction.', array('@instance_id' => $transaction->instance_id)), 'error');
    $form_state['rebuild'] = TRUE;
  }
  elseif ($api_call_success = commerce_nets_process_transaction($order, $payment_method, $transaction, 'CREDIT', commerce_currency_decimal_to_amount($form_state['values']['amount'], $transaction->currency_code))) {

    // Update amount - store how much has been captured - credited.
    $transaction->amount = _commerce_nets_amount($transaction, 'CREDIT', TRUE);

    // If there's nothing more to be credited or captured we can change
    // transaction status.
    if (_commerce_nets_amount($transaction, 'CREDIT') == 0 && _commerce_nets_amount($transaction, 'CAPTURE') == 0) {
      // Set transaction status to what Nets told us it did.
      $transaction->status = commerce_nets_transaction_status_from_nets_operation('CAPTURE');
    }

    // Don't change transaction status.
    commerce_payment_transaction_save($transaction);

    drupal_set_message(t('Transaction has been credited.', array()));
    $form_state['redirect'] = $uri['path'];
  }
  else {
    drupal_set_message(t('Unable to credit transaction. %message', array('%message' => $transaction->payload['%message'])), 'error');
    $form_state['rebuild'] = TRUE;
  }
}
