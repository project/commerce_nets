<?php

/**
 * @file
 * Contains the classes used in commerce_nets.
 *
 * @todo Documentation??
 */

/**
 * CommerceNetsTerminal class.
 */
class CommerceNetsTerminal {

  public $AutoAuth;
  public $PaymentMethodList;
  public $Language;
  public $OrderDescription;
  public $RedirectOnError;
  public $RedirectUrl;

  /**
   * Class constructor.
   */
  public function __construct(
    $auto_auth,
    $payment_method_list,
    $language,
    $order_description,
    $redirect_on_error,
    $redirect_url
  ) {
    $this->AutoAuth = $auto_auth;
    $this->PaymentMethodList = $payment_method_list;
    $this->Language = $language;
    $this->OrderDescription = $order_description;
    $this->RedirectOnError = $redirect_on_error;
    $this->RedirectUrl = $redirect_url;
  }

}
/**
 * CommerceNetsRecurring class.
 */
class CommerceNetsRecurring {

  public $ExpiryDate;
  public $Frequency;
  public $Type;
  public $PanHash;

  /**
   * Class constructor.
   */
  public function __construct(
    $expiry_date,
    $frequency,
    $type,
    $pan_hash
  ) {
    $this->ExpiryDate       = $expiry_date;
    $this->Frequency        = $frequency;
    $this->Type             = $type;
    $this->PanHash          = $pan_hash;
  }

}
/**
 * CommerceNetsRegisterRequest class.
 */
class CommerceNetsRegisterRequest {

  public $AvtaleGiro;
  public $CardInfo;
  public $Customer;
  public $Description;
  public $DnBNorDirectPayment;
  public $Environment;
  public $MicroPayment;
  public $Order;
  public $Recurring;
  public $ServiceType;
  public $Terminal;
  public $TransactionId;
  public $TransactionReconRef;

  /**
   * Class constructor.
   */
  public function __construct(
    $avtale_giro,
    $card_info,
    $customer,
    $description,
    $dnb_nor_direct_payment,
    $environment,
    $micro_payment,
    $order,
    $recurring,
    $service_type,
    $terminal,
    $transaction_id,
    $transaction_recon_ref
  ) {
    $this->AvtaleGiro = $avtale_giro;
    $this->CardInfo = $card_info;
    $this->Customer = $customer;
    $this->Description = $description;
    $this->DnBNorDirectPayment = $dnb_nor_direct_payment;
    $this->Environment = $environment;
    $this->MicroPayment = $micro_payment;
    $this->Order = $order;
    $this->Recurring = $recurring;
    $this->ServiceType = $service_type;
    $this->Terminal = $terminal;
    $this->TransactionId = $transaction_id;
    $this->TransactionReconRef = $transaction_recon_ref;
  }

}
/**
 * CommerceNetsQueryRequest class.
 */
class CommerceNetsQueryRequest {

  public $TransactionId;

  /**
   * Class constructor.
   */
  public function __construct($transaction_id) {
    $this->TransactionId = $transaction_id;
  }

}
/**
 * CommerceNetsProcessRequest class.
 */
class CommerceNetsProcessRequest {

  public $Description;
  public $Operation;
  public $TransactionAmount;
  public $TransactionId;
  public $TransactionReconRef;

  /**
   * Class constructor.
   */
  public function __construct(
    $description,
    $operation,
    $transaction_amount,
    $transaction_id,
    $transaction_recon_ref
  ) {
    $this->Description = $description;
    $this->Operation = $operation;
    $this->TransactionAmount = $transaction_amount;
    $this->TransactionId = $transaction_id;
    $this->TransactionReconRef = $transaction_recon_ref;
  }

}
/**
 * CommerceNetsOrder class.
 */
class CommerceNetsOrder {

  public $Amount;
  public $CurrencyCode;
  public $Force3DSecure;
  public $Goods;
  public $OrderNumber;
  public $UpdateStoredPaymentInfo;

  /**
   * Class constructor.
   */
  public function __construct(
    $amount,
    $currency_code,
    $force_3d_secure,
    $goods,
    $order_number,
    $update_stored_payment_info
  ) {
    $this->Amount = $amount;
    $this->CurrencyCode = $currency_code;
    $this->Force3DSecure = $force_3d_secure;
    $this->Goods = $goods;
    $this->OrderNumber = $order_number;
    $this->UpdateStoredPaymentInfo = $update_stored_payment_info;
  }

}
/**
 * CommerceNetsItem class.
 */
class CommerceNetsItem {

  public $Amount;
  public $ArticleNumber;
  public $Discount;
  public $Handling;
  public $IsVatIncluded;
  public $Quantity;
  public $Shipping;
  public $Title;
  public $VAT;

  /**
   * Class constructor.
   */
  public function __construct(
    $amount,
    $article_number,
    $discount,
    $handling,
    $is_vat_included,
    $quantity,
    $shipping,
    $title,
    $vat
  ) {
    $this->Amount = $amount;
    $this->ArticleNumber = $article_number;
    $this->Discount = $discount;
    $this->Handling = $handling;
    $this->IsVatIncluded = $is_vat_included;
    $this->Quantity = $quantity;
    $this->Shipping = $shipping;
    $this->Title = $title;
    $this->VAT = $vat;
  }

}
/**
 * CommerceNetsEnvironment class.
 */
class CommerceNetsEnvironment {

  public $Language;
  public $OS;
  public $WebServicePlatform;

  /**
   * Class constructor.
   */
  public function __construct(
    $language,
    $os,
    $web_service_platform
  ) {
    $this->Language = $language;
    $this->OS = $os;
    $this->WebServicePlatform = $web_service_platform;
  }

}
/**
 * CommerceNetsCustomer class.
 */
class CommerceNetsCustomer {

  public $Address1;
  public $Address2;
  public $CompanyName;
  public $CompanyRegistrationNumber;
  public $Country;
  public $CustomerNumber;
  public $Email;
  public $FirstName;
  public $LastName;
  public $PhoneNumber;
  public $Postcode;
  public $SocialSecurityNumber;
  public $Town;

  /**
   * Class constructor.
   */
  public function __construct(
    $address1,
    $address2,
    $company_name,
    $company_registration_number,
    $country,
    $customer_number,
    $email,
    $first_name,
    $last_name,
    $phone_number,
    $postcode,
    $social_security_number,
    $town
  ) {
    $this->Address1 = $address1;
    $this->Address2 = $address2;
    $this->CompanyName = $company_name;
    $this->CompanyRegistrationNumber = $company_registration_number;
    $this->Country = $country;
    $this->CustomerNumber = $customer_number;
    $this->Email = $email;
    $this->FirstName = $first_name;
    $this->LastName = $last_name;
    $this->PhoneNumber = $phone_number;
    $this->Postcode = $postcode;
    $this->SocialSecurityNumber = $social_security_number;
    $this->Town = $town;
  }

}
/**
 * CommerceNetsArrayOfItem class.
 */
class CommerceNetsArrayOfItem {

  public $Item;

  /**
   * Class constructor.
   */
  public function __construct($item) {
    $this->Item = $item;
  }

}
