
COMMERCE NETS PAYMENT METHOD
----------------------------

Payment method for Drupal Commerce.
Implements payment using the norwegian Nets payment service (former BBS). 
Allows for Visa & mastercard transactions. Supports recurring payment with 
stored HASH from Nets.

Takes you to off-site payment page.

INSTALLATION
------------

-Enable the module.
-Edit the new Nets payment rule, and add the NETS token and authentication code.


