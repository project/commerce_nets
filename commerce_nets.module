<?php

/**
 * @file
 * Enables users to pay using Nets Terminal.
 */

/**
 * Implements hook_menu().
 */
function commerce_nets_menu() {
  $items = array();

  // Capture.
  $items['admin/commerce/orders/%commerce_order/payment/%commerce_payment_transaction/commerce-nets-capture'] = array(
    'title' => 'Capture',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_nets_admin_capture', 3, 5),
    'access callback' => 'commerce_nets_transaction_operation_access',
    'access arguments' => array('CAPTURE', 3, 5),
    'type' => MENU_LOCAL_ACTION,
    'context' => MENU_CONTEXT_INLINE,
    'weight' => 5,
    'file' => 'commerce_nets.admin.inc',
  );

  // Credit.
  $items['admin/commerce/orders/%commerce_order/payment/%commerce_payment_transaction/commerce-nets-credit'] = array(
    'title' => 'Credit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_nets_admin_credit', 3, 5),
    'access callback' => 'commerce_nets_transaction_operation_access',
    'access arguments' => array('CREDIT', 3, 5),
    'type' => MENU_LOCAL_ACTION,
    'context' => MENU_CONTEXT_INLINE,
    'weight' => 6,
    'file' => 'commerce_nets.admin.inc',
  );

  // Cancel.
  $items['admin/commerce/orders/%commerce_order/payment/%commerce_payment_transaction/commerce-nets-anull'] = array(
    'title' => 'Anull',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_nets_admin_cancel', 3, 5),
    'access callback' => 'commerce_nets_transaction_operation_access',
    'access arguments' => array('ANNUL', 3, 5),
    'type' => MENU_LOCAL_ACTION,
    'context' => MENU_CONTEXT_INLINE,
    'weight' => 7,
    'file' => 'commerce_nets.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_nets_commerce_payment_method_info() {
  $payment_methods = array();

  $payment_methods['commerce_nets'] = array(
    'title' => t('Nets Terminal'),
    'description' => t('Enables users to pay using Nets Terminal.'),

    'display_title' => t('Credit card'),
    'short_title' => 'Nets',

    // Start as enabled.
    'active' => TRUE,

    // Disable payments in order’s Payment tab.
    'terminal' => FALSE,

    // Sends user offsite.
    'offsite' => TRUE,

    // Automaticly redirect user offsite.
    'offsite_autoredirect' => TRUE,

    // File to be included
    'file' => 'commerce_nets.inc',
  );

  return $payment_methods;
}

/**
 * Implements hook_commerce_payment_transaction_status_info().
 *
 * Note: We never add money to the total.
 * Reason for this is we want success (money is to be paid to the shop) to be the
 * only status that actually changes the order. This way when we change our
 * transactions to the new ones we get 'intended' funtionality for free.
 * This might not be the best in the long run.
 */
function commerce_nets_commerce_payment_transaction_status_info() {
  $statuses = array();

  $statuses['credit'] = array(
    'status' => 'credit',
    'title' => t('Credit'),
    'icon' => drupal_get_path('module', 'commerce_payment') . '/theme/icon-failure.png',
    'total' => FALSE,
  );

  $statuses['registered'] = array(
    'status' => 'registered',
    'title' => t('Registered'),
    'icon' => drupal_get_path('module', 'commerce_payment') . '/theme/icon-pending.png',
    'total' => FALSE,
  );

  $statuses['authorize'] = array(
    'status' => 'authorize',
    'title' => t('Authorize'),
    'icon' => drupal_get_path('module', 'commerce_nets') . '/theme/icon-authorized.png',
    'total' => FALSE,
  );

  $statuses['annull'] = array(
    'status' => 'annull',
    'title' => t('Annull'),
    'icon' => drupal_get_path('module', 'commerce_payment') . '/theme/icon-failure.png',
    'total' => FALSE,
  );

  return $statuses;
}

/**
 * Checks if a transaction meets requirements for an $operation.
 *
 * @param string $operation
 *   Operation to perform: CAPTURE/CREDIT/ANNUL.
 * @param object $order
 *   Commerce Order (loaded) the transaction belongs to.
 * @param object $transaction
 *   Commerce Payment Transaction (loaded) to perform operation for.
 *
 * @return bool
 *   TRUE if allowed to perform operation, FALSE otherwise.
 */
function commerce_nets_transaction_operation_access($operation, $order, $transaction) {

  // Don't bother other transactions.
  if ($transaction->payment_method != 'commerce_nets') {
    return FALSE;
  }

  // Basic check to see if user can administer payments.
  if (!user_access('update payments')) {
    return FALSE;
  }

  // Failure (local) or Cancel (Nets) and we can't do much.
  if ($transaction->status == 'failure' || $transaction->remote_status == 'Cancel') {
    return FALSE;
  }

  // NOTE: success == SALE. This can be Credited or Anulled i guess.
  switch ($operation) {

    case 'ANNUL':
      return $transaction->status == 'authorize' && $transaction->remote_status == 'OK';
    break;

    case 'CAPTURE':
      module_load_include('inc', 'commerce_nets');
      return in_array($transaction->status, array('success', 'authorize')) && _commerce_nets_amount($transaction, 'CAPTURE') > 0;
      break;

    case 'CREDIT':
      module_load_include('inc', 'commerce_nets');
      return in_array($transaction->status, array('success', 'authorize')) && _commerce_nets_amount($transaction, 'CREDIT') > 0;
      break;

    default:
      break;
  }

  // We don't know what is asked of us.
  return FALSE;
}

/**
 * Implements hook_entity_view_alter().
 */
function commerce_nets_entity_view_alter(&$build, $type) {
  if ($type == 'commerce_payment_transaction' && $build['#entity']->payment_method == 'commerce_nets') {
    module_load_include('inc', 'commerce_nets');
    $build['commerce_nets'] = commerce_nets_get_payment_summary($build['#entity']);
  }
}
